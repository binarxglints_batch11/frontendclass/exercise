// useState= obj dari react
import React, {useState} from 'react';

// destructuring call useState
// const {useState} = React;

//function based component
const AppCounter = () => {
    //setting up state count dan setCount, dimana set awal = 0
    const [count, setCount] = useState(0)


    const increment = () => {
        setCount(count+1);
    }

    const decrement = () => {
        setCount(count-1)
    }

    const reset = () => {
        setCount(0)
    }

    return(
        <>
            <hr></hr>
            <p>Click {count} times</p>
            <button onClick={increment}>Decrement</button>
            <button onClick={decrement}>Decrement</button>
            <button onClick={reset}>Decrement</button>
        </>
    );
}

export {AppCounter};