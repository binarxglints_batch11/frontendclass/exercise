import React, {useState} from 'react'
import './index.css'

export const StepCounter = () => {
  const [step, setStepCounter] = useState(0);

  const stepCounter = () => {
    setStepCounter(step+1);
  }

  return (
    <main>
      <hr></hr>
      <span aria-hidden>👟</span>
      <div>You've walked {step} steps so far today.</div>
      <button onClick={stepCounter}>Record a Step</button>
    </main>
  );
};

