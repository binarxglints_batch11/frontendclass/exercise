import React from 'react';
import './App.css';

// useRef diamnil methodsnya saja dari object React
const {useRef} = React

export const LoginForm = () => {
    //password sama username ini diambil valuenya dgn method useRef()
    const usernameRef = useRef();
    const passwordRef = useRef();

    console.log(usernameRef);
    const handleSubmit = event => {
        // to prevent reseting input value
        event.preventDefault();
        console.log(usernameRef);
        console.log(usernameRef.current.value, passwordRef.current.value)
    };

    return(
        <form onSubmit={handleSubmit}>
            <label htmlFor="username">Username</label>
            <input ref={usernameRef} id="username" />

            <label htmlFor="password">Password</label>
            <input ref={passwordRef} id="password" />

            <button type="submit">Log In</button>
        </form>
    );
}

