import React, {useState} from 'react';

export function MultipleState() {
    const [water, setOuncesWater] = useState(8);
    const [coffee, setGramsCoffee] = useState(13);

    const coffeeMaker = () => {
        setGramsCoffee(coffee+1);
    }

    const waterMaker = () => {
        setOuncesWater(water+1);
    }
    
    return(
        <>
        <hr></hr>
        <h1>Coffee Maker </h1>
        <span>☕️</span>
        <h2>Coffee: {coffee}</h2>
        <button onClick={coffeeMaker}>Nambah Kopi</button>
        <h1>Water Maker</h1>
        <span>🥤</span>
        <h2>Coffee: {water}</h2>
        <button onClick={waterMaker}>Nambah Air</button>
        </>
    );
}