import React, {useState} from 'react';

function useStateComplexBefore() {
  const [coffee, setGramsCoffee] = useState(13);
  const [water, setOuncesWater] = useState(8);

  return (
    <div>
      <hr></hr>
      <h1>Water Maker</h1>
      <span aria-hidden>☕</span>
      <h2>Water</h2>
      <button
        onClick={() => setOuncesWater(water + 1)}
      >
        {water} oz
      </button>
      <div>
      <h1>Coffee Maker</h1>
      <span aria-hidden>🥤</span>
      <button
        onClick={() => setGramsCoffee(coffee + 1)}
      >
        {coffee} g
      </button>
      </div>
    </div>
  );
}

export default useStateComplexBefore