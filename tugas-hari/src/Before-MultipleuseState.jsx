import './countDownLifeCycle.css'

function MultipleStateBefore() {
  return (
    <div>
      <h1>Coffee Maker</h1>
      <span aria-hidden>☕</span>
      <h2>Water</h2>
      <h2>Coffee</h2>
    </div>
  );
}

export default MultipleStateBefore