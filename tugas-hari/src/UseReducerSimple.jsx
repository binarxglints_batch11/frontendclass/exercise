import './useReducerSimple.css';
// 1. dont forget input useReducer from react
import React, {useReducer} from 'react';

//2. create the reducer with state and action, and add the switch statement
// const [light, dispath] = useReducer(tombol, true);
//3. logic of the switch statement is if the case on light true, if the case off light is false then return back to state
const tombol = (state, action) => {
  switch(action) {
    case 1:
      return true;
    case 0:
      return false;
    default:
      return state;
  }
}

function RoomBefore() {
  //4. declare the useReducer variable (light and dispatch)
  const [light, dispath] = useReducer(tombol,false);

  return (
    //5. Perhaps you can see the className from the css which one should be call when the case on or off
    <div className={`room ${light ? 'lit' : 'unlit'}`}>
      <h1>Room</h1>
      {/* 6. Dispatch the case when the button clicked */}
      <button onClick={() => dispath(1)}>ON</button>
      <button onClick={() => dispath(0)}>OFF</button>
    </div>
  );
}

export default RoomBefore