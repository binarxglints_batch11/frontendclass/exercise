// import {AppCounter} from './cobaHooks';
// import MultipleStateBefore from './Before-MultipleuseState';
// import {MultipleState} from './MultipleState';
// import {StepCounter} from './StepCounter';
// import UseStateComplexBefore from './UseStateComplex';
// import {LoginForm} from './After-useRefDomNode'
// import RoomBefore from './UseReducerSimple';
// import Counter from './After-refactorClassComponent';
import Countdown from './After-countdownModule';
import AppRefactBefore from './After-refactorLifecycleClassComponent';
import './App.css';


function App() {
  return (
    <div className="App">
      <AppRefactBefore/>
      <Countdown />
      {/* <Counter /> */}
      {/* <RoomBefore/> */}
      {/* <LoginForm />
      <AppCounter />
      <MultipleStateBefore/>
      <MultipleState/>
      <UseStateComplexBefore/>
      <StepCounter /> */}
    </div>
  );
}

export default App;
