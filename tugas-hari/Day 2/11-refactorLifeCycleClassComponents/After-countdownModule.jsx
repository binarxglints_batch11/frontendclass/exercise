import React, {useState, useEffect, useRef} from 'react';

//0. Just read the algorithm from Countdown Class Component
//1. set-up the useState second and setSeconds with startTime parameter as the default value
//2. set-up the timer by using useRef()
//3. create useEffect statement to clearInterval when the seconds === 0 (input is seconds)
//4. create useEffect statement again to handle setup Countdown (input is startTime)
//5. return to div with {seconds} variable

const Countdown = (props) => {
  const timer = null;
  const [seconds, setSeconds] = useState(0);

  const {startTime} = props;

  useEffect(() => {
    setupCountdown(startTime)
  }, [])

  const setupCountdown = (startTime) => {
    setSeconds( startTime );
    if (startTime > 0) {
      const timer = setInterval(() => {
        if (seconds > 0) {
          setSeconds(seconds - 1
          );
        } else {
          clearInterval(timer);
        }
      }, 1000);
    }
  }
  const prevValue = useRef(startTime);


  useEffect(() => {
    if (startTime !== prevValue) {
      clearInterval(timer);
      setupCountdown(startTime);
    }
  });

  useEffect(() => {
    return clearInterval(timer);
  }, []);

  
    return(
    <>
    <div>{seconds}</div>
    </>
    ); 
}

export default Countdown