# Todo List App build with React Hooks

## Introduction
The goal is to create To Do List App and this App will have 3 main components:
- Header that labels the To Do list. This is just a basic application identifier
- A list to display each to do item.
- A form that adds a To Do task item to the list. The default complete should be set to false.

## Step
1. Create a React Application
- ` yarn create react-app todo-list ` OR
- ` npx create-react-app todo-list `

2. Clean AppJS like This
```
import React from 'react';
import './App.css';
 
function App() {
 return (
   <div className="App">
    	Todo App Helloo!
   </div>
 );
}
 
export default App;
```

3. Create Header Component
4. Use the Mock Data to test Application
5. Read list of todos and display
6. Toggle Task Completion (by using CSS `line-decoration: line-through`)
7. Delete Complete Task
8. Add tasks with form component
